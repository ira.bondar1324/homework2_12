"use strict";

const wrapperBtn = document.querySelector(".btn-wrapper");
const arrBtn = wrapperBtn.children;

function changeBackColor(){
    for(let i = 0;i<arrBtn.length;i++){
        arrBtn[i].style.backgroundColor = "#000000";
    }
}

for (const btn of arrBtn) {
    document.addEventListener("keydown",(e)=>{
        if(e.key.toUpperCase()===btn.textContent.toUpperCase()){
            changeBackColor();
            btn.style.backgroundColor = "blue";
        }
    });
}